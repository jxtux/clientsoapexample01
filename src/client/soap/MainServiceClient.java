package client.soap;

import client.soap.generate.ServiceUser;
import client.soap.generate.ServiceUserImplService;
import client.soap.generate.SimpleException;
import client.soap.generate.User;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class MainServiceClient {

    public static void main(String[] args) {
        /*GET NAME*/
        try {
            System.out.println("Response: " + getMessage("antonio5x"));
        } catch (Exception e) {
        }

        /* GET USER */
        Date today = new Date();
        XMLGregorianCalendar xmlDate = toXMLGregorianCalendar(today);

        User u = new User();
        u.setId(500);
        u.setName("jose antonio 5x");
        u.setEdad(xmlDate);

        User u_ = getUser(u);
        System.out.println("\n /****USER******/");
        System.out.println("Datos: {user:" + u_.getId() + ",name:" + u_.getName() + ",edad:" + u_.getEdad());

        /*GET LIST USER*/
        List<User> list = getListUser(10);

        System.out.println("\n /****LISTA DE USUARIOS******/");
        for (User user : list) {
            System.out.println("Datos: {user:" + user.getId() + ",name:" + user.getName() + ",edad:" + user.getEdad());
        }
    }

    public static String getMessage(String name) throws SimpleException {
        ServiceUser port = getServiice_();

        return port.obtenerMessageName(name);
    }

    public static User getUser(User u) {
        ServiceUser port = getServiice_();

        return port.getUserParametroObject(u);
    }

    public static List<User> getListUser(Integer cant) {
        ServiceUser port = getServiice_();

        List<User> list = port.getListUser(cant);

        return list;
    }

    public static ServiceUser getServiice_() {
        ServiceUserImplService service = new ServiceUserImplService();

        return service.getServiceUserImplPort();
    }

    public static XMLGregorianCalendar toXMLGregorianCalendar(Date date) {
        GregorianCalendar gCalendar = new GregorianCalendar();
        gCalendar.setTime(date);
        XMLGregorianCalendar xmlCalendar = null;
        try {
            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        } catch (DatatypeConfigurationException ex) {
            // Logger.getLogger(StringReplace.class.getName()).log(Level.SEVERE, null, ex);
        }
        return xmlCalendar;
    }

}
